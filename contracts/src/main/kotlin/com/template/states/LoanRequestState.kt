package com.template.states

import com.template.contracts.LoanRequestContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

// *********
// * State *
// *********
@BelongsToContract(LoanRequestContract::class)
class LoanRequestState(val requestValue: Int,
                                 val requester: Party,
                                 val bank: Party,
                                 val loanStatus: Int = 0,
                                 override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {
                override val participants get() = listOf(requester, bank)
}